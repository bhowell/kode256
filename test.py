import code128
import os

p = os.path.normcase
path = os.path

data = [ "Hello World",         # simple text
         "12345678901",         # simple number, odd digits
         "01100101",            # simple number, even digets
         "some longer text~~",  # tilde
         "Holläröödudiliüöö",   # not-ASCII characters
         "00xx000x0000xx00000", # usage of code C (1)
         "0000X000000X0000",    # usage of code C (2)
         "Hello\nWorld",        # ASCII control character --> Code A (1)
         "Hello\nWORLD1234",    # Code A and numbers
         "HELLO\nWorld",        # Switch from Code A to Code B (lower case)
         "0815\nHELLO\nW123456\n\torld", # all charsets together
         "~2H~4dllo", "Welt"]   # use FNC2 to divide data into two codes
                                # and FNC4 to encode a non-ASCII character

if not path.exists(p("./out/")): os.mkdir("out")

for i,d in enumerate(data):
    print(d)
    code128.image(d,200,4,True).save(p("out/%02d.png" % i))  # output with PIL
    with open(p("out/%02d.svg" % i), "w") as f:
        f.write(code128.svg(d,200,4,True))                   # output as SVG
    print()
